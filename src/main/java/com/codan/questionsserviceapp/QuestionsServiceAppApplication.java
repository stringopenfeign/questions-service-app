package com.codan.questionsserviceapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
public class QuestionsServiceAppApplication {
	public static void main(String[] args) {
		SpringApplication.run(QuestionsServiceAppApplication.class, args);
	}

}
